import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../provider/color_change_model.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<MyColorChangeModel>(
      builder: (_, myColorChangeModel, __) {
        return GestureDetector(
          onTap: () => myColorChangeModel.onChange(),
          child: Container(
            color: myColorChangeModel.randomColor,
            child: Align(
              alignment: Alignment.center,
              child: Text(
                'Hey there!',
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
